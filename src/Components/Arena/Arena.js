import React, { memo } from "react";

import Slide from "./Slide";

import { Wrapper } from "./Arena.module.css";

const getCoord = (size, deg) => {
  const rad = (deg / 360) * 3.14;
  const r = size | 2;
  const x = r * Math.cos(rad);
  const y = r * Math.sin(rad);
  return { x, y };
};

const getSizes = (length, size) => {
  const rotate = 360 / length;
  const minRotate = rotate < 60 ? rotate : 60;
  const a = getCoord(size, 0);
  const b = getCoord(size, minRotate);
  const pathSize = Math.sqrt(
    Math.pow(Math.max(a.x, b.x) - Math.min(a.x, b.x), 2) +
      Math.pow(Math.max(a.y, b.y) - Math.min(a.y, b.y), 2)
  );
  const halfSize = size / 2;
  const shapeR =
    (pathSize / 2) *
    Math.sqrt((2 * halfSize - pathSize) / (2 * halfSize + pathSize));
  const shapeSize = shapeR * 2;
  const shapeCoord = halfSize - shapeSize;

  return { rotate, shapeSize, shapeCoord };
};

const Arena = ({ size = 0, slides = [] }) => {
  const { rotate, shapeSize, shapeCoord } = getSizes(slides.length, size);

  return (
    <div
      className={Wrapper}
      style={{
        width: size,
        height: size
      }}
    >
      {slides.map((slide, index) => {
        return (
          <Slide
            index={index}
            shapeSize={shapeSize}
            rotate={rotate * index}
            backRotate={rotate * index}
            shapeCoord={shapeCoord}
            {...slide}
          />
        );
      })}
    </div>
  );
};

export default memo(Arena);
