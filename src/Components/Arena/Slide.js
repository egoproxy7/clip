import React, { memo } from "react";
import { Wrapper, Shape } from "./Slide.module.css";

const Slide = ({ index, shapeSize, shapeCoord, rotate, backRotate }) => {
  return (
    <div className={Wrapper} style={{ transform: `rotate(${rotate}deg)` }}>
      <div
        className={Shape}
        style={{
          bottom: shapeCoord,
          left: -(shapeSize / 2),
          transform: `rotate(-${backRotate}deg)`,
          width: shapeSize,
          height: shapeSize,
          zIndex: 100 + index
        }}
      >
        {index}
      </div>
    </div>
  );
};

export default memo(Slide);
