import React from "react";
import ReactResizeDetector from "react-resize-detector";

import Arena from "./Components/Arena";
// import logo from "./logo.svg";
import "./App.css";

const getSlidesConfig = num => {
  const arr = [];
  for (let i = 0; i < num; i += 1) {
    arr.push({ name: i });
  }

  return arr;
};

const slidesConfig = getSlidesConfig(6);

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <ReactResizeDetector handleWidth handleHeight>
        {({ width, height }) => {
          return width && height ? (
            <Arena slides={slidesConfig} size={Math.min(width, height)} />
          ) : (
            <div />
          );
        }}
      </ReactResizeDetector>
    </div>
  );
}

export default App;
